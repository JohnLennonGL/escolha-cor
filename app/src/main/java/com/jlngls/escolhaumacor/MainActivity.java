package com.jlngls.escolhaumacor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends Activity {


    private RadioGroup radioGroup;
    private RadioButton corEscolhida;
    private Button botaoSalvar;
    private static final String ARQUIVO_PREFERENCIA = "AqrPreferencia";
    private ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = findViewById(R.id.RadioGroupID);
        botaoSalvar = findViewById(R.id.BotaoSalvarID);
        constraintLayout = findViewById(R.id.backGroundID);

        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idRadioGroupEscolhido = radioGroup.getCheckedRadioButtonId();

                if (idRadioGroupEscolhido > 0) {
                    corEscolhida = findViewById(idRadioGroupEscolhido);

                    //SEMPRE VAI SER ASSIM
                    SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
String cores = corEscolhida.getText().toString();
                    editor.putString("corEscolhida", cores);
                    editor.commit();

                    setBackGround(cores);
                }

            }

        });
        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
if(sharedPreferences.contains("corEscolhida")){
    String salvarCor =  sharedPreferences.getString("corEscolhida","Azul");
    setBackGround(salvarCor);
}
    }

    private void setBackGround(String cor){
        if (cor.equals("Azul")){
            constraintLayout.setBackgroundColor(Color.parseColor("#0055ff"));
        }
        else if(cor.equals("Vermelho")){
            constraintLayout.setBackgroundColor(Color.parseColor("#ff3200"));
        }
        else if(cor.equals("Verde")){
            constraintLayout.setBackgroundColor(Color.parseColor("#72ff00"));
        }
        else if(cor.equals("Amarelo")){
            constraintLayout.setBackgroundColor(Color.parseColor("#ffe900"));
        }
    }


}
